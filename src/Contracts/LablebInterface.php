<?php

namespace Lableb\Contracts;

/**
 * Interface  LablebInterface
 *
 * @author  Lableb Team  <support@lableb.com>
 */
interface LablebInterface
{

    /**
     * Indexes documents on Lableb
     *
     * @param indexName - index name on Lableb
     * @param documents - a single document or an array of documents
     *
     * @return Array
     * @throws LablebException
     */
    public function index($indexName, $documents);

    /**
     * Searches documents on Lableb
     *
     * @param indexName - index name on Lableb
     * @param query - an associative array of search query parameters
     * @param handler - an optional parameter which refers to the search handler
     *
     * @return Array
     * @throws LablebException
     */
    public function search($indexName, $documents);

    /**
     * Searches for autocomplete suggestions
     *
     * @param indexName - index name on Lableb
     * @param options - an associative array of autocomplete query parameters
     * @param handler - name of the search handler [OPTIONAL]
     *
     * @return Array
     * @throws LablebException
     */
    public function autocomplete($indexName, $documents);

    /**
     * Fetches related artciles to the one specified in $query
     *
     * @param indexName - index name on Lableb
     * @param query - an associative array describing a document
     * @param handler - suggestion handler name [OPTIONAL]
     *
     * @return Array
     * @throws LablebException
     */
    public function recommend($indexName, $documents);

    /**
     * Deletes an indexed document on Lableb
     *
     * @param indexName - index name on Lableb
     * @param id - id of the document to be deleted
     *
     * @return Array
     * @throws LablebException
     */
    public function delete($indexName, $id);

    /**
     * Submits a search result click feedback
     *
     * @param indexName - index name on Lableb
     * @param params - an associative array of feedback parameters
     * @param handler - what search handler was used in the search results
     *
     * @return Array
     * @throws LablebException
     */
    public function submitSearchFeedback($indexName, $params, $handler = 'default');

    /**
     * Submits autocomplete suggestion click
     *
     * @param indexName - index name on Lableb
     * @param params - an associative array of request body
     * @param handler - used autocomplete handler name [OPTIONAL]
     *
     * @return Array
     * @throws LablebException
     */
    public function submitAutocompleteFeedback($indexName, $params, $handler = 'suggest');

    /**
     * Submits a recommended articles click
     *
     * @param indexName - index name on Lableb
     * @param source - source document
     * @param target - target (clicked) document
     * @param handler - used suggestion handler name [OPTIONAL]
     *
     * @return Array
     * @throws LablebException
     */
    public function submitRecommendationFeedback($indexName, $source, $target, $handler = 'recommend');

}
